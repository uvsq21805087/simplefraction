package exo3_12_1;

public class ChaineCryptee {
	private String msgCl;
	private int decalage;
	
	private ChaineCryptee(String msgCl, int decalage){
		this.msgCl = msgCl;
		this.decalage = decalage;
	}
	
	public static ChaineCryptee deCryptee(String msgCh, int nbr) {
		try {
			String msgCl = "";	
			for(int i=0; i< msgCh.length(); i++) {
				msgCl+=decaleCaractere(msgCh.charAt(i), 26-nbr);
			};
			return new ChaineCryptee(msgCl, nbr);
		}
		catch(NullPointerException e) {
			System.out.println("Chaine null");
		}
		return null;
	}
	
	public static ChaineCryptee deEnClair(String msgCl, int nbr) {
		return new ChaineCryptee(msgCl, nbr);
	}
	
	public String getMsgCl() {
		return msgCl;
	}

	public void setMsgCl(String msgCl) {
		this.msgCl = msgCl;
	}

	public int getDecalage() {
		return decalage;
	}

	public void setDecalage(int decalage) {
		this.decalage = decalage;
	}

	public String decrypte() {
		return msgCl;
	}
	
	public String crypte() {		
		try {
			String msgCh = "";	
			for(int i=0; i< this.msgCl.length(); i++) {
				msgCh+=decaleCaractere(this.msgCl.charAt(i), this.decalage);
			};
			
			return msgCh;
		}
		catch(NullPointerException e) {
			System.out.println("Chaine null");
		}
		return null;

	}
	
	private static char decaleCaractere(char c, int decalage) {
        return (c < 'A' || c > 'Z')? c : (char)(((c - 'A' + decalage) % 26) + 'A');
    }
}
